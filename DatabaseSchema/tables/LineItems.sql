-------------------------------------------------------------------------------------
-- Note: All data in this file was generated at https://mockaroo.com/.
-- Thank you for existing Mockaroo!
-------------------------------------------------------------------------------------

DROP TABLE IF EXISTS LineItems;
create table LineItems
(
    line_item_id    BIGINT IDENTITY PRIMARY KEY,
    order_id        BIGINT,
    prescription_id INT,
    quantity        INT,
    CONSTRAINT FK_LineItem_Order FOREIGN KEY (order_id) REFERENCES Orders (order_id),
    CONSTRAINT FK_LineItem_Prescription FOREIGN KEY (prescription_id) REFERENCES Prescriptions (prescription_id)
);

SET IDENTITY_INSERT LineItems ON;
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (1, 263, 49, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (2, 73, 13, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (3, 269, 71, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (4, 154, 3, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (5, 46, 44, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (6, 127, 52, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (7, 42, 27, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (8, 58, 51, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (9, 126, 91, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (10, 270, 82, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (11, 163, 43, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (12, 127, 83, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (13, 111, 56, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (14, 122, 12, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (15, 205, 59, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (16, 267, 80, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (17, 138, 93, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (18, 33, 72, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (19, 162, 73, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (20, 293, 12, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (21, 107, 59, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (22, 149, 19, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (23, 33, 73, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (24, 3, 57, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (25, 173, 27, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (26, 56, 21, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (27, 288, 63, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (28, 103, 59, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (29, 284, 35, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (30, 288, 98, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (31, 297, 37, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (32, 86, 72, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (33, 133, 67, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (34, 77, 89, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (35, 30, 61, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (36, 50, 86, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (37, 226, 26, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (38, 200, 13, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (39, 257, 91, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (40, 107, 17, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (41, 123, 51, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (42, 49, 13, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (43, 196, 47, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (44, 246, 21, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (45, 180, 2, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (46, 174, 70, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (47, 17, 30, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (48, 91, 65, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (49, 111, 1, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (50, 217, 3, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (51, 229, 25, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (52, 290, 9, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (53, 1, 79, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (54, 207, 29, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (55, 2, 89, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (56, 44, 22, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (57, 179, 30, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (58, 199, 91, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (59, 155, 67, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (60, 49, 55, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (61, 198, 21, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (62, 215, 46, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (63, 44, 62, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (64, 21, 35, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (65, 80, 8, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (66, 207, 13, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (67, 10, 15, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (68, 243, 93, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (69, 173, 69, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (70, 256, 61, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (71, 113, 57, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (72, 214, 64, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (73, 41, 99, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (74, 248, 15, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (75, 206, 23, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (76, 150, 2, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (77, 39, 64, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (78, 54, 45, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (79, 153, 75, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (80, 108, 71, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (81, 202, 48, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (82, 84, 25, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (83, 181, 79, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (84, 265, 31, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (85, 154, 84, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (86, 77, 47, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (87, 76, 79, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (88, 243, 77, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (89, 70, 55, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (90, 257, 88, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (91, 256, 66, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (92, 279, 70, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (93, 90, 23, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (94, 177, 51, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (95, 168, 26, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (96, 210, 62, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (97, 151, 43, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (98, 14, 88, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (99, 126, 68, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (100, 94, 93, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (101, 139, 90, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (102, 159, 90, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (103, 195, 90, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (104, 241, 15, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (105, 120, 67, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (106, 292, 14, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (107, 55, 4, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (108, 268, 58, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (109, 251, 29, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (110, 13, 55, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (111, 246, 31, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (112, 219, 84, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (113, 219, 57, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (114, 216, 4, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (115, 119, 82, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (116, 76, 8, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (117, 166, 57, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (118, 95, 84, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (119, 241, 70, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (120, 35, 43, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (121, 131, 56, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (122, 284, 86, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (123, 280, 69, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (124, 27, 28, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (125, 263, 64, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (126, 93, 22, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (127, 172, 6, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (128, 225, 5, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (129, 250, 38, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (130, 31, 32, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (131, 252, 15, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (132, 132, 32, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (133, 18, 92, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (134, 277, 48, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (135, 204, 8, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (136, 120, 12, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (137, 13, 85, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (138, 65, 14, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (139, 15, 44, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (140, 120, 45, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (141, 216, 92, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (142, 274, 70, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (143, 215, 59, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (144, 229, 66, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (145, 232, 62, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (146, 136, 78, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (147, 286, 19, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (148, 111, 69, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (149, 36, 96, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (150, 126, 27, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (151, 264, 26, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (152, 269, 31, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (153, 228, 91, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (154, 214, 21, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (155, 273, 67, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (156, 97, 40, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (157, 44, 64, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (158, 259, 64, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (159, 24, 33, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (160, 201, 6, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (161, 174, 87, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (162, 220, 74, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (163, 150, 35, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (164, 293, 26, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (165, 279, 13, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (166, 152, 55, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (167, 17, 89, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (168, 210, 74, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (169, 89, 60, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (170, 67, 82, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (171, 100, 96, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (172, 88, 75, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (173, 288, 76, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (174, 129, 49, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (175, 184, 73, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (176, 172, 29, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (177, 35, 25, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (178, 61, 59, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (179, 45, 17, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (180, 226, 52, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (181, 232, 78, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (182, 39, 98, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (183, 144, 70, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (184, 49, 35, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (185, 215, 50, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (186, 275, 2, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (187, 64, 86, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (188, 31, 14, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (189, 13, 15, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (190, 237, 54, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (191, 82, 29, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (192, 61, 99, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (193, 204, 71, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (194, 8, 57, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (195, 291, 62, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (196, 195, 62, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (197, 98, 70, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (198, 285, 8, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (199, 98, 29, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (200, 26, 88, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (201, 269, 2, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (202, 217, 88, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (203, 81, 21, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (204, 5, 97, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (205, 40, 27, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (206, 179, 44, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (207, 300, 58, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (208, 279, 16, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (209, 85, 57, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (210, 48, 6, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (211, 178, 72, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (212, 220, 3, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (213, 121, 44, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (214, 196, 96, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (215, 237, 3, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (216, 299, 87, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (217, 232, 5, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (218, 109, 89, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (219, 249, 77, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (220, 137, 19, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (221, 298, 66, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (222, 234, 91, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (223, 270, 14, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (224, 274, 23, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (225, 3, 20, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (226, 263, 43, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (227, 239, 9, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (228, 42, 13, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (229, 194, 29, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (230, 214, 55, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (231, 90, 60, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (232, 202, 75, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (233, 114, 24, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (234, 98, 36, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (235, 289, 10, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (236, 280, 9, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (237, 78, 5, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (238, 205, 64, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (239, 272, 67, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (240, 68, 44, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (241, 96, 42, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (242, 230, 94, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (243, 228, 65, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (244, 258, 65, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (245, 77, 89, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (246, 33, 48, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (247, 269, 20, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (248, 42, 96, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (249, 165, 90, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (250, 5, 35, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (251, 285, 90, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (252, 281, 57, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (253, 227, 42, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (254, 296, 39, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (255, 295, 79, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (256, 46, 37, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (257, 218, 13, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (258, 66, 94, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (259, 240, 69, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (260, 119, 33, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (261, 194, 92, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (262, 135, 74, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (263, 72, 58, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (264, 11, 99, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (265, 231, 67, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (266, 29, 69, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (267, 256, 54, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (268, 24, 49, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (269, 131, 81, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (270, 53, 23, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (271, 208, 82, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (272, 23, 65, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (273, 246, 90, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (274, 163, 29, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (275, 24, 38, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (276, 47, 4, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (277, 164, 17, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (278, 137, 96, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (279, 130, 2, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (280, 166, 56, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (281, 206, 3, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (282, 208, 81, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (283, 95, 46, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (284, 51, 53, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (285, 45, 1, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (286, 206, 20, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (287, 32, 34, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (288, 91, 67, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (289, 181, 2, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (290, 217, 93, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (291, 106, 95, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (292, 213, 32, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (293, 279, 81, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (294, 241, 97, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (295, 76, 3, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (296, 214, 89, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (297, 277, 76, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (298, 276, 83, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (299, 148, 3, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (300, 251, 55, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (301, 272, 99, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (302, 113, 36, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (303, 70, 98, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (304, 214, 61, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (305, 191, 18, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (306, 243, 44, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (307, 135, 100, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (308, 169, 7, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (309, 260, 41, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (310, 157, 60, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (311, 267, 2, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (312, 197, 6, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (313, 230, 36, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (314, 179, 20, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (315, 186, 27, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (316, 152, 38, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (317, 127, 75, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (318, 109, 98, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (319, 130, 68, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (320, 200, 73, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (321, 91, 49, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (322, 217, 13, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (323, 54, 68, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (324, 227, 60, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (325, 79, 55, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (326, 50, 7, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (327, 203, 17, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (328, 215, 63, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (329, 262, 5, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (330, 203, 43, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (331, 77, 64, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (332, 227, 83, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (333, 58, 53, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (334, 76, 98, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (335, 268, 92, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (336, 290, 54, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (337, 275, 66, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (338, 123, 11, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (339, 38, 95, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (340, 52, 52, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (341, 163, 71, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (342, 253, 33, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (343, 189, 19, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (344, 83, 23, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (345, 252, 35, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (346, 116, 60, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (347, 192, 44, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (348, 104, 48, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (349, 253, 80, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (350, 249, 33, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (351, 116, 36, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (352, 112, 77, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (353, 230, 95, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (354, 119, 94, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (355, 74, 34, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (356, 43, 49, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (357, 280, 97, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (358, 132, 52, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (359, 98, 29, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (360, 128, 18, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (361, 243, 92, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (362, 202, 74, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (363, 74, 93, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (364, 116, 68, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (365, 292, 15, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (366, 97, 85, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (367, 131, 2, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (368, 259, 44, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (369, 177, 48, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (370, 237, 28, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (371, 131, 36, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (372, 295, 9, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (373, 5, 59, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (374, 52, 85, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (375, 118, 51, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (376, 85, 68, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (377, 137, 22, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (378, 130, 91, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (379, 141, 16, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (380, 60, 15, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (381, 270, 34, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (382, 61, 68, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (383, 133, 71, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (384, 180, 59, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (385, 180, 75, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (386, 108, 82, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (387, 148, 72, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (388, 299, 79, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (389, 231, 19, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (390, 238, 100, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (391, 80, 75, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (392, 79, 67, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (393, 25, 34, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (394, 192, 40, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (395, 4, 48, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (396, 18, 25, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (397, 176, 5, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (398, 14, 16, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (399, 233, 68, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (400, 278, 60, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (401, 160, 61, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (402, 108, 75, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (403, 86, 22, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (404, 214, 97, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (405, 265, 12, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (406, 262, 88, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (407, 181, 81, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (408, 79, 100, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (409, 182, 89, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (410, 121, 54, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (411, 7, 18, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (412, 217, 97, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (413, 176, 18, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (414, 89, 98, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (415, 117, 5, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (416, 236, 27, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (417, 59, 32, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (418, 173, 98, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (419, 32, 61, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (420, 125, 51, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (421, 228, 36, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (422, 283, 51, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (423, 266, 23, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (424, 151, 28, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (425, 188, 10, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (426, 55, 23, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (427, 63, 30, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (428, 167, 17, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (429, 193, 97, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (430, 125, 60, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (431, 202, 46, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (432, 132, 99, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (433, 210, 29, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (434, 117, 33, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (435, 207, 84, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (436, 215, 57, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (437, 4, 35, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (438, 246, 8, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (439, 110, 35, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (440, 212, 63, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (441, 127, 47, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (442, 258, 94, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (443, 148, 48, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (444, 126, 11, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (445, 108, 60, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (446, 230, 53, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (447, 119, 5, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (448, 100, 51, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (449, 92, 41, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (450, 250, 54, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (451, 298, 37, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (452, 102, 90, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (453, 43, 89, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (454, 73, 33, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (455, 255, 45, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (456, 242, 71, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (457, 15, 47, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (458, 115, 51, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (459, 210, 63, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (460, 285, 97, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (461, 237, 83, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (462, 251, 24, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (463, 63, 87, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (464, 128, 67, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (465, 101, 6, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (466, 203, 64, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (467, 159, 55, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (468, 268, 99, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (469, 199, 5, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (470, 82, 34, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (471, 78, 57, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (472, 57, 79, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (473, 70, 51, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (474, 299, 49, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (475, 257, 43, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (476, 94, 66, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (477, 218, 48, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (478, 222, 65, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (479, 291, 99, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (480, 166, 22, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (481, 91, 65, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (482, 222, 88, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (483, 252, 33, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (484, 24, 43, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (485, 123, 19, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (486, 103, 75, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (487, 256, 31, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (488, 52, 53, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (489, 223, 43, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (490, 144, 14, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (491, 21, 46, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (492, 226, 14, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (493, 148, 39, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (494, 11, 5, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (495, 300, 42, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (496, 78, 46, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (497, 70, 26, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (498, 1, 40, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (499, 236, 72, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (500, 205, 42, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (501, 74, 56, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (502, 51, 77, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (503, 134, 71, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (504, 47, 60, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (505, 269, 94, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (506, 146, 70, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (507, 157, 92, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (508, 233, 80, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (509, 290, 88, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (510, 6, 65, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (511, 229, 41, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (512, 245, 62, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (513, 99, 21, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (514, 201, 96, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (515, 16, 22, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (516, 234, 76, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (517, 282, 55, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (518, 81, 45, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (519, 221, 44, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (520, 177, 13, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (521, 115, 22, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (522, 139, 68, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (523, 236, 89, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (524, 292, 73, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (525, 56, 21, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (526, 277, 5, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (527, 163, 50, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (528, 216, 59, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (529, 85, 33, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (530, 186, 51, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (531, 63, 38, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (532, 49, 77, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (533, 112, 73, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (534, 70, 38, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (535, 269, 81, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (536, 173, 73, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (537, 99, 93, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (538, 100, 10, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (539, 30, 43, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (540, 212, 49, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (541, 2, 59, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (542, 158, 7, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (543, 5, 8, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (544, 172, 70, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (545, 59, 23, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (546, 213, 9, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (547, 273, 46, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (548, 4, 54, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (549, 191, 60, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (550, 51, 72, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (551, 4, 56, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (552, 282, 56, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (553, 290, 96, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (554, 271, 60, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (555, 111, 48, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (556, 36, 99, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (557, 108, 23, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (558, 66, 88, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (559, 218, 11, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (560, 79, 40, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (561, 76, 7, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (562, 10, 9, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (563, 40, 48, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (564, 132, 16, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (565, 137, 39, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (566, 185, 86, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (567, 295, 73, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (568, 176, 58, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (569, 200, 50, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (570, 2, 13, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (571, 296, 27, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (572, 64, 56, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (573, 68, 52, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (574, 49, 27, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (575, 101, 9, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (576, 45, 80, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (577, 113, 61, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (578, 244, 29, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (579, 164, 11, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (580, 271, 10, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (581, 9, 84, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (582, 74, 11, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (583, 169, 88, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (584, 267, 59, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (585, 188, 78, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (586, 78, 53, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (587, 142, 87, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (588, 95, 47, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (589, 80, 63, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (590, 86, 57, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (591, 277, 77, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (592, 142, 65, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (593, 78, 79, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (594, 190, 36, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (595, 107, 9, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (596, 63, 7, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (597, 279, 53, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (598, 229, 100, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (599, 117, 3, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (600, 87, 55, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (601, 270, 83, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (602, 261, 80, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (603, 300, 55, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (604, 210, 14, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (605, 164, 59, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (606, 3, 14, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (607, 277, 20, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (608, 204, 71, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (609, 247, 60, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (610, 6, 98, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (611, 40, 49, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (612, 145, 39, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (613, 188, 42, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (614, 10, 94, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (615, 139, 51, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (616, 42, 79, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (617, 14, 15, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (618, 149, 64, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (619, 110, 15, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (620, 19, 12, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (621, 108, 47, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (622, 135, 99, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (623, 61, 88, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (624, 201, 11, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (625, 59, 19, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (626, 56, 29, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (627, 158, 39, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (628, 214, 41, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (629, 284, 50, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (630, 256, 5, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (631, 10, 39, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (632, 135, 14, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (633, 49, 30, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (634, 97, 71, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (635, 29, 8, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (636, 25, 77, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (637, 295, 92, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (638, 231, 35, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (639, 213, 87, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (640, 27, 21, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (641, 257, 50, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (642, 79, 67, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (643, 163, 87, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (644, 101, 66, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (645, 175, 28, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (646, 142, 29, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (647, 287, 12, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (648, 198, 42, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (649, 193, 90, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (650, 9, 42, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (651, 50, 74, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (652, 27, 62, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (653, 109, 77, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (654, 211, 100, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (655, 157, 94, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (656, 197, 5, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (657, 148, 64, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (658, 126, 39, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (659, 88, 82, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (660, 68, 37, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (661, 207, 47, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (662, 44, 54, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (663, 272, 32, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (664, 288, 68, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (665, 139, 16, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (666, 203, 67, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (667, 212, 100, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (668, 22, 6, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (669, 60, 95, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (670, 68, 78, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (671, 242, 59, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (672, 84, 41, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (673, 59, 54, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (674, 130, 6, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (675, 141, 2, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (676, 109, 90, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (677, 76, 33, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (678, 295, 38, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (679, 215, 8, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (680, 31, 39, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (681, 87, 1, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (682, 30, 74, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (683, 53, 82, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (684, 39, 87, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (685, 66, 75, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (686, 190, 7, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (687, 294, 53, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (688, 141, 75, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (689, 39, 9, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (690, 82, 92, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (691, 201, 47, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (692, 259, 47, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (693, 124, 77, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (694, 141, 98, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (695, 195, 5, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (696, 69, 3, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (697, 241, 50, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (698, 67, 64, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (699, 15, 92, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (700, 131, 97, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (701, 20, 95, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (702, 109, 55, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (703, 290, 57, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (704, 2, 3, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (705, 145, 25, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (706, 171, 93, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (707, 31, 93, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (708, 45, 58, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (709, 93, 79, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (710, 299, 94, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (711, 50, 96, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (712, 241, 54, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (713, 222, 91, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (714, 215, 64, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (715, 194, 88, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (716, 194, 90, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (717, 94, 16, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (718, 71, 84, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (719, 7, 80, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (720, 11, 5, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (721, 247, 46, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (722, 211, 75, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (723, 87, 1, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (724, 46, 62, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (725, 255, 29, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (726, 204, 79, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (727, 244, 29, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (728, 38, 11, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (729, 158, 78, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (730, 94, 21, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (731, 140, 17, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (732, 80, 61, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (733, 62, 72, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (734, 51, 97, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (735, 32, 30, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (736, 30, 43, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (737, 283, 97, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (738, 15, 53, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (739, 144, 31, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (740, 128, 49, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (741, 98, 69, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (742, 297, 96, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (743, 268, 83, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (744, 76, 48, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (745, 241, 66, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (746, 229, 1, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (747, 12, 11, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (748, 85, 87, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (749, 256, 28, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (750, 3, 76, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (751, 194, 40, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (752, 292, 95, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (753, 294, 20, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (754, 193, 64, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (755, 34, 12, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (756, 105, 25, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (757, 297, 14, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (758, 212, 46, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (759, 218, 25, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (760, 40, 74, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (761, 145, 56, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (762, 132, 80, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (763, 233, 65, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (764, 37, 75, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (765, 252, 41, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (766, 170, 37, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (767, 183, 44, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (768, 122, 42, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (769, 204, 82, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (770, 218, 21, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (771, 276, 87, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (772, 256, 83, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (773, 156, 57, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (774, 256, 42, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (775, 27, 45, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (776, 166, 82, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (777, 98, 75, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (778, 3, 18, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (779, 55, 91, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (780, 252, 80, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (781, 126, 35, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (782, 168, 20, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (783, 131, 14, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (784, 115, 36, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (785, 86, 3, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (786, 67, 55, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (787, 167, 87, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (788, 199, 35, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (789, 251, 45, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (790, 226, 98, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (791, 265, 49, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (792, 268, 2, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (793, 241, 79, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (794, 163, 11, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (795, 7, 52, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (796, 22, 37, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (797, 70, 97, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (798, 72, 89, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (799, 120, 99, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (800, 60, 42, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (801, 196, 80, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (802, 116, 83, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (803, 205, 73, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (804, 45, 40, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (805, 106, 85, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (806, 22, 84, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (807, 238, 27, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (808, 262, 39, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (809, 224, 21, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (810, 251, 9, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (811, 93, 38, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (812, 225, 99, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (813, 22, 73, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (814, 132, 47, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (815, 41, 98, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (816, 134, 38, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (817, 77, 30, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (818, 142, 30, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (819, 5, 37, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (820, 184, 84, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (821, 228, 95, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (822, 269, 97, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (823, 175, 39, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (824, 161, 47, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (825, 227, 22, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (826, 271, 31, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (827, 136, 5, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (828, 160, 91, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (829, 258, 41, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (830, 207, 98, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (831, 48, 16, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (832, 21, 3, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (833, 244, 41, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (834, 122, 87, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (835, 5, 48, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (836, 1, 82, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (837, 69, 58, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (838, 25, 54, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (839, 283, 51, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (840, 207, 73, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (841, 244, 41, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (842, 258, 24, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (843, 24, 76, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (844, 161, 44, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (845, 277, 32, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (846, 136, 95, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (847, 203, 8, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (848, 2, 91, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (849, 297, 63, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (850, 34, 64, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (851, 289, 11, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (852, 40, 74, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (853, 77, 14, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (854, 100, 50, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (855, 127, 61, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (856, 297, 67, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (857, 126, 92, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (858, 156, 54, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (859, 101, 90, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (860, 181, 61, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (861, 174, 36, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (862, 250, 83, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (863, 225, 91, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (864, 222, 7, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (865, 201, 89, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (866, 144, 34, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (867, 43, 58, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (868, 98, 100, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (869, 141, 19, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (870, 98, 92, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (871, 13, 49, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (872, 58, 52, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (873, 26, 89, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (874, 153, 83, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (875, 60, 99, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (876, 146, 47, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (877, 35, 38, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (878, 185, 24, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (879, 95, 42, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (880, 22, 17, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (881, 132, 24, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (882, 37, 33, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (883, 99, 14, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (884, 66, 59, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (885, 254, 99, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (886, 25, 40, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (887, 43, 67, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (888, 117, 96, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (889, 53, 6, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (890, 284, 49, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (891, 29, 3, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (892, 129, 87, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (893, 231, 89, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (894, 173, 18, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (895, 210, 99, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (896, 112, 96, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (897, 25, 6, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (898, 38, 5, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (899, 47, 95, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (900, 191, 77, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (901, 18, 83, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (902, 201, 22, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (903, 282, 1, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (904, 112, 45, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (905, 230, 18, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (906, 261, 1, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (907, 124, 65, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (908, 104, 87, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (909, 93, 90, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (910, 289, 22, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (911, 134, 87, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (912, 140, 27, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (913, 180, 28, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (914, 79, 39, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (915, 245, 85, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (916, 202, 3, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (917, 103, 47, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (918, 293, 6, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (919, 229, 7, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (920, 16, 41, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (921, 43, 5, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (922, 54, 31, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (923, 172, 92, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (924, 146, 49, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (925, 14, 60, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (926, 103, 25, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (927, 157, 60, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (928, 111, 49, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (929, 200, 65, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (930, 249, 52, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (931, 196, 98, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (932, 23, 70, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (933, 260, 6, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (934, 118, 91, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (935, 6, 37, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (936, 108, 14, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (937, 217, 31, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (938, 115, 25, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (939, 298, 53, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (940, 54, 47, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (941, 57, 53, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (942, 135, 8, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (943, 20, 82, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (944, 9, 23, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (945, 1, 79, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (946, 137, 27, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (947, 272, 24, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (948, 133, 91, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (949, 66, 52, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (950, 100, 12, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (951, 259, 9, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (952, 148, 57, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (953, 290, 33, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (954, 86, 29, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (955, 214, 8, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (956, 110, 41, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (957, 7, 25, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (958, 265, 48, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (959, 229, 15, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (960, 158, 11, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (961, 256, 77, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (962, 147, 64, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (963, 78, 19, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (964, 282, 59, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (965, 172, 67, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (966, 161, 14, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (967, 59, 28, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (968, 32, 6, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (969, 291, 91, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (970, 46, 22, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (971, 81, 44, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (972, 44, 39, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (973, 45, 12, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (974, 106, 93, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (975, 30, 7, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (976, 131, 79, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (977, 223, 3, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (978, 191, 72, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (979, 78, 100, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (980, 34, 35, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (981, 80, 34, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (982, 299, 98, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (983, 136, 32, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (984, 50, 27, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (985, 112, 87, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (986, 221, 64, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (987, 28, 31, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (988, 155, 95, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (989, 95, 87, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (990, 105, 100, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (991, 240, 59, 3);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (992, 176, 50, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (993, 263, 6, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (994, 24, 15, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (995, 211, 56, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (996, 6, 38, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (997, 150, 44, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (998, 251, 28, 2);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (999, 261, 10, 1);
insert into LineItems (line_item_id, order_id, prescription_id, quantity)
values (1000, 13, 94, 3);

SET IDENTITY_INSERT LineItems OFF;