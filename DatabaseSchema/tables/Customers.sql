-------------------------------------------------------------------------------------
-- Note: All data in this file is fake. It was generated at https://mockaroo.com/.
-- Thank you for existing Mockaroo!
-------------------------------------------------------------------------------------

create table Customers
(
    customer_id BIGINT IDENTITY PRIMARY KEY,
    first_name  VARCHAR(50),
    last_name   VARCHAR(50),
    email       VARCHAR(50),
    address     VARCHAR(50),
    city        VARCHAR(50),
    state       VARCHAR(50)
);

SET IDENTITY_INSERT Customers ON

insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (1, 'Melesa', 'Cairns', 'mcairns0@usa.gov', '14120 Norway Maple Street', 'Inglewood', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (2, 'Cassie', 'Brickstock', 'cbrickstock1@sourceforge.net', '5 Birchwood Place', 'Young America', 'MN');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (3, 'Kip', 'Sutlieff', 'ksutlieff2@jigsy.com', '4 Portage Parkway', 'Las Vegas', 'NV');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (4, 'Bonnibelle', 'Bubeer', 'bbubeer3@wsj.com', '95 Pennsylvania Trail', 'Abilene', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (5, 'Erich', 'Scirman', 'escirman4@feedburner.com', '66600 Mayfield Road', 'Phoenix', 'AZ');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (6, 'Jo-anne', 'Goodbar', 'jgoodbar5@guardian.co.uk', '6 Judy Point', 'Pueblo', 'CO');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (7, 'Lina', 'Atlay', 'latlay6@amazon.com', '166 Delladonna Pass', 'Lexington', 'KY');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (8, 'Ozzy', 'Berntssen', 'oberntssen7@nyu.edu', '32 Jay Park', 'Sacramento', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (9, 'Felisha', 'Kares', 'fkares8@cafepress.com', '3781 Buena Vista Hill', 'Charlotte', 'NC');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (10, 'Vladimir', 'Massey', 'vmassey9@nytimes.com', '9 Fuller Park', 'Sioux Falls', 'SD');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (11, 'Fayette', 'Claxton', 'fclaxtona@uiuc.edu', '03 New Castle Center', 'Atlanta', 'GA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (12, 'Mia', 'Sandels', 'msandelsb@thetimes.co.uk', '6068 Fremont Drive', 'Dallas', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (13, 'Fina', 'Feaver', 'ffeaverc@wsj.com', '816 4th Center', 'Young America', 'MN');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (14, 'Hanna', 'Romaine', 'hromained@xrea.com', '94 Stone Corner Hill', 'Honolulu', 'HI');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (15, 'Myrle', 'Tallboy', 'mtallboye@dot.gov', '0 Doe Crossing Way', 'Las Vegas', 'NV');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (16, 'Clair', 'Von Welden', 'cvonweldenf@bigcartel.com', '6 Coolidge Avenue', 'New York City', 'NY');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (17, 'Bendick', 'Bullimore', 'bbullimoreg@dagondesign.com', '10 Johnson Avenue', 'Washington', 'DC');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (18, 'Adda', 'Klaus', 'aklaush@jiathis.com', '79 Birchwood Place', 'Flint', 'MI');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (19, 'Shandeigh', 'Digby', 'sdigbyi@delicious.com', '4483 Veith Pass', 'Norfolk', 'VA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (20, 'Cyb', 'Greschik', 'cgreschikj@rakuten.co.jp', '066 Elmside Lane', 'Pittsburgh', 'PA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (21, 'Shanda', 'Chesshyre', 'schesshyrek@newyorker.com', '301 Hooker Road', 'Harrisburg', 'PA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (22, 'Cory', 'Jacquet', 'cjacquetl@ca.gov', '05477 Lakeland Drive', 'Bridgeport', 'CT');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (23, 'Nappy', 'Stollard', 'nstollardm@ning.com', '52 Merry Way', 'Annapolis', 'MD');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (24, 'Eddie', 'Pedroni', 'epedronin@spiegel.de', '96 Parkside Circle', 'Los Angeles', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (25, 'Eleen', 'Middle', 'emiddleo@eepurl.com', '0695 Bunker Hill Circle', 'Brooklyn', 'NY');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (26, 'Wren', 'Sarre', 'wsarrep@yellowbook.com', '55 Muir Hill', 'New Castle', 'PA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (27, 'Ivonne', 'O''Neal', 'ionealq@google.com.br', '6 Kipling Drive', 'Helena', 'MT');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (28, 'Gypsy', 'Triplow', 'gtriplowr@archive.org', '2 Gulseth Lane', 'Austin', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (29, 'Lorinda', 'Gillam', 'lgillams@dagondesign.com', '3 Judy Plaza', 'Round Rock', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (30, 'Kevan', 'Jordanson', 'kjordansont@ask.com', '8 Monument Street', 'Lafayette', 'LA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (31, 'Vanni', 'Mead', 'vmeadu@techcrunch.com', '67690 Nelson Alley', 'Pittsburgh', 'PA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (32, 'Rici', 'Vlies', 'rvliesv@taobao.com', '3 Rieder Street', 'Salt Lake City', 'UT');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (33, 'Demetri', 'Shepherd', 'dshepherdw@histats.com', '2098 Derek Alley', 'Las Vegas', 'NV');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (34, 'Brenna', 'Lightwing', 'blightwingx@columbia.edu', '3508 Gerald Point', 'Los Angeles', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (35, 'Eve', 'Phelip', 'ephelipy@unc.edu', '33604 Farmco Court', 'San Antonio', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (36, 'Wilmar', 'Blackborough', 'wblackboroughz@comcast.net', '61 Dayton Terrace', 'Philadelphia', 'PA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (37, 'Jermain', 'Kamen', 'jkamen10@vinaora.com', '165 Continental Pass', 'Paterson', 'NJ');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (38, 'Corly', 'Toombs', 'ctoombs11@usatoday.com', '073 Moland Road', 'Fort Pierce', 'FL');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (39, 'Bent', 'Aysh', 'baysh12@clickbank.net', '558 Sherman Street', 'New Orleans', 'LA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (40, 'Rocky', 'Lakes', 'rlakes13@google.com.br', '1584 Petterle Place', 'Olympia', 'WA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (41, 'Jasen', 'Sivier', 'jsivier14@usa.gov', '2366 Cody Parkway', 'New York City', 'NY');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (42, 'Horatia', 'Mapstone', 'hmapstone15@g.co', '8 Harbort Place', 'Oklahoma City', 'OK');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (43, 'Aldridge', 'Teather', 'ateather16@phpbb.com', '8117 Swallow Avenue', 'Dallas', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (44, 'Benedicto', 'Pickering', 'bpickering17@chronoengine.com', '89520 Kings Center', 'Madison', 'WI');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (45, 'Mariellen', 'Puttan', 'mputtan18@scribd.com', '452 Express Trail', 'Santa Clara', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (46, 'Dillie', 'Illing', 'dilling19@mayoclinic.com', '8451 Hansons Park', 'Staten Island', 'NY');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (47, 'Dov', 'Huscroft', 'dhuscroft1a@canalblog.com', '34 Pawling Trail', 'Van Nuys', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (48, 'Jeramie', 'Folliss', 'jfolliss1b@umich.edu', '0489 Alpine Junction', 'Atlanta', 'GA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (49, 'Rhona', 'Pedersen', 'rpedersen1c@geocities.jp', '76497 Onsgard Pass', 'Gaithersburg', 'MD');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (50, 'Rita', 'Pitrasso', 'rpitrasso1d@ucoz.ru', '82 Drewry Junction', 'Santa Rosa', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (51, 'Maurise', 'Hamshaw', 'mhamshaw1e@delicious.com', '4046 Clarendon Point', 'Orlando', 'FL');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (52, 'Genevra', 'Hedworth', 'ghedworth1f@pagesperso-orange.fr', '44506 Messerschmidt Road', 'Detroit', 'MI');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (53, 'Giffie', 'Landells', 'glandells1g@simplemachines.org', '92 Stone Corner Alley', 'Salt Lake City', 'UT');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (54, 'Winfield', 'Ashworth', 'washworth1h@yelp.com', '1735 Garrison Circle', 'Los Angeles', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (55, 'Phaidra', 'Naisby', 'pnaisby1i@msn.com', '87 Declaration Hill', 'Levittown', 'PA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (56, 'Blondie', 'Handlin', 'bhandlin1j@reddit.com', '7 Dryden Terrace', 'Ogden', 'UT');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (57, 'Marlo', 'Mecozzi', 'mmecozzi1k@hao123.com', '229 Jana Trail', 'New Orleans', 'LA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (58, 'Skippy', 'Easterling', 'seasterling1l@chronoengine.com', '5 1st Circle', 'San Jose', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (59, 'Ive', 'Whyman', 'iwhyman1m@jigsy.com', '265 Pennsylvania Parkway', 'Alexandria', 'VA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (60, 'Fredra', 'Lehr', 'flehr1n@ibm.com', '70178 Oakridge Way', 'Beaumont', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (61, 'Bayard', 'Simester', 'bsimester1o@tuttocitta.it', '156 Texas Avenue', 'Boise', 'ID');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (62, 'Ebony', 'Hatherall', 'ehatherall1p@xrea.com', '6187 Schlimgen Crossing', 'Detroit', 'MI');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (63, 'Oona', 'Kopmann', 'okopmann1q@census.gov', '93 Helena Center', 'Akron', 'OH');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (64, 'Agathe', 'Maffy', 'amaffy1r@ed.gov', '87151 Red Cloud Lane', 'Birmingham', 'AL');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (65, 'Saxon', 'Whatsize', 'swhatsize1s@ftc.gov', '746 American Center', 'Fresno', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (66, 'Sile', 'Luis', 'sluis1t@fema.gov', '783 Johnson Park', 'Austin', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (67, 'Shae', 'Sifflett', 'ssifflett1u@biglobe.ne.jp', '71 Valley Edge Parkway', 'Providence', 'RI');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (68, 'Raeann', 'Boorn', 'rboorn1v@fda.gov', '9 Amoth Point', 'Topeka', 'KS');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (69, 'Marysa', 'Thorby', 'mthorby1w@zdnet.com', '1 Warbler Park', 'Gilbert', 'AZ');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (70, 'Troy', 'Dudbridge', 'tdudbridge1x@oaic.gov.au', '4 Pond Court', 'Philadelphia', 'PA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (71, 'Dicky', 'Greeson', 'dgreeson1y@prnewswire.com', '33861 Forest Dale Avenue', 'Richmond', 'VA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (72, 'Nerissa', 'Sennett', 'nsennett1z@washington.edu', '78 Southridge Way', 'Springfield', 'IL');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (73, 'Darelle', 'Sumner', 'dsumner20@google.com.au', '2785 Hudson Place', 'Saint Paul', 'MN');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (74, 'Zed', 'Bastone', 'zbastone21@tamu.edu', '00 Rockefeller Crossing', 'Omaha', 'NE');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (75, 'Ardith', 'Vasiliu', 'avasiliu22@amazon.com', '02 Messerschmidt Pass', 'Atlanta', 'GA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (76, 'Imogen', 'Pull', 'ipull23@soundcloud.com', '36 Carpenter Terrace', 'Birmingham', 'AL');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (77, 'Nappy', 'Ruby', 'nruby24@dot.gov', '0 Warbler Drive', 'Atlanta', 'GA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (78, 'Theda', 'Cartlidge', 'tcartlidge25@freewebs.com', '7 Vermont Street', 'Albuquerque', 'NM');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (79, 'Alaric', 'Whitely', 'awhitely26@bloglines.com', '7891 Acker Court', 'Schenectady', 'NY');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (80, 'Mercy', 'Daborn', 'mdaborn27@icio.us', '80 Rutledge Terrace', 'Greenville', 'SC');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (81, 'Adamo', 'Nuccitelli', 'anuccitelli28@typepad.com', '9 Bluejay Avenue', 'Maple Plain', 'MN');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (82, 'Jarvis', 'Jedraszczyk', 'jjedraszczyk29@paypal.com', '364 Mayfield Court', 'Springfield', 'MA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (83, 'Helenelizabeth', 'Jikylls', 'hjikylls2a@usnews.com', '6518 Dawn Road', 'Herndon', 'VA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (84, 'Yasmin', 'Cattle', 'ycattle2b@whitehouse.gov', '7 Farragut Court', 'Detroit', 'MI');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (85, 'Skye', 'Flower', 'sflower2c@spiegel.de', '40 Maywood Crossing', 'Burbank', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (86, 'Sandy', 'Janaszkiewicz', 'sjanaszkiewicz2d@msu.edu', '8854 Nevada Road', 'Portsmouth', 'VA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (87, 'Christa', 'Ouldcott', 'couldcott2e@google.es', '833 Meadow Ridge Road', 'Fort Lauderdale', 'FL');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (88, 'Dulsea', 'Wyley', 'dwyley2f@biglobe.ne.jp', '4302 Helena Place', 'Berkeley', 'CA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (89, 'Winny', 'Sydall', 'wsydall2g@123-reg.co.uk', '5960 Hansons Park', 'Kent', 'WA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (90, 'Jayme', 'Escoffrey', 'jescoffrey2h@webs.com', '75 Di Loreto Park', 'Austin', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (91, 'Conrado', 'Falconar', 'cfalconar2i@tiny.cc', '05 Colorado Street', 'Montgomery', 'AL');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (92, 'Lorrayne', 'Maccraw', 'lmaccraw2j@multiply.com', '52 Pennsylvania Drive', 'Cincinnati', 'OH');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (93, 'Aubry', 'Lawson', 'alawson2k@nbcnews.com', '097 Merchant Parkway', 'West Hartford', 'CT');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (94, 'Deeann', 'Osboldstone', 'dosboldstone2l@cocolog-nifty.com', '2819 Waxwing Street', 'Houston', 'TX');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (95, 'Melany', 'Ferres', 'mferres2m@washington.edu', '59 Briar Crest Way', 'Jamaica', 'NY');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (96, 'Tallulah', 'McTeggart', 'tmcteggart2n@princeton.edu', '87 Heffernan Place', 'Salt Lake City', 'UT');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (97, 'Erina', 'Dwelley', 'edwelley2o@bbb.org', '252 Badeau Parkway', 'Lynchburg', 'VA');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (98, 'Nicky', 'Janczewski', 'njanczewski2p@shop-pro.jp', '18070 Farmco Crossing', 'Kansas City', 'MO');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (99, 'Tudor', 'Camber', 'tcamber2q@theatlantic.com', '02 Commercial Pass', 'Jamaica', 'NY');
insert into Customers (customer_id, first_name, last_name, email, address, city, state)
values (100, 'Lyn', 'Hauck', 'lhauck2r@rambler.ru', '86 Prairie Rose Crossing', 'Jamaica', 'NY');

SET IDENTITY_INSERT Customers OFF