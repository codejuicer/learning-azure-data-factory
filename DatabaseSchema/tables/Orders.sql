-------------------------------------------------------------------------------------
-- Note: All data in this file was generated at https://mockaroo.com/.
-- Thank you for existing Mockaroo!
-------------------------------------------------------------------------------------

CREATE TABLE Orders
(
    order_id    BIGINT IDENTITY PRIMARY KEY,
    order_date  DATETIME2 NOT NULL,
    customer_id BIGINT    NOT NULL,
    CONSTRAINT FK_Order_CustomerId FOREIGN KEY (customer_id) REFERENCES Customers (customer_id)
);

SET IDENTITY_INSERT Orders ON;

insert into Orders (order_id, order_date, customer_id)
values (1, '3/24/2019', 38);
insert into Orders (order_id, order_date, customer_id)
values (2, '12/9/2018', 95);
insert into Orders (order_id, order_date, customer_id)
values (3, '2/5/2019', 53);
insert into Orders (order_id, order_date, customer_id)
values (4, '7/5/2019', 43);
insert into Orders (order_id, order_date, customer_id)
values (5, '11/18/2018', 22);
insert into Orders (order_id, order_date, customer_id)
values (6, '4/3/2019', 30);
insert into Orders (order_id, order_date, customer_id)
values (7, '5/30/2019', 41);
insert into Orders (order_id, order_date, customer_id)
values (8, '10/23/2019', 60);
insert into Orders (order_id, order_date, customer_id)
values (9, '3/25/2019', 92);
insert into Orders (order_id, order_date, customer_id)
values (10, '3/13/2019', 39);
insert into Orders (order_id, order_date, customer_id)
values (11, '8/5/2019', 53);
insert into Orders (order_id, order_date, customer_id)
values (12, '5/7/2019', 20);
insert into Orders (order_id, order_date, customer_id)
values (13, '12/17/2018', 35);
insert into Orders (order_id, order_date, customer_id)
values (14, '12/29/2018', 97);
insert into Orders (order_id, order_date, customer_id)
values (15, '3/24/2019', 17);
insert into Orders (order_id, order_date, customer_id)
values (16, '12/5/2018', 76);
insert into Orders (order_id, order_date, customer_id)
values (17, '3/31/2019', 92);
insert into Orders (order_id, order_date, customer_id)
values (18, '2/14/2019', 64);
insert into Orders (order_id, order_date, customer_id)
values (19, '10/31/2019', 67);
insert into Orders (order_id, order_date, customer_id)
values (20, '11/3/2018', 54);
insert into Orders (order_id, order_date, customer_id)
values (21, '11/11/2018', 95);
insert into Orders (order_id, order_date, customer_id)
values (22, '10/11/2019', 69);
insert into Orders (order_id, order_date, customer_id)
values (23, '5/10/2019', 17);
insert into Orders (order_id, order_date, customer_id)
values (24, '1/25/2019', 10);
insert into Orders (order_id, order_date, customer_id)
values (25, '2/27/2019', 63);
insert into Orders (order_id, order_date, customer_id)
values (26, '10/30/2019', 79);
insert into Orders (order_id, order_date, customer_id)
values (27, '7/21/2019', 98);
insert into Orders (order_id, order_date, customer_id)
values (28, '5/26/2019', 81);
insert into Orders (order_id, order_date, customer_id)
values (29, '11/1/2018', 69);
insert into Orders (order_id, order_date, customer_id)
values (30, '10/9/2019', 78);
insert into Orders (order_id, order_date, customer_id)
values (31, '2/12/2019', 31);
insert into Orders (order_id, order_date, customer_id)
values (32, '7/27/2019', 96);
insert into Orders (order_id, order_date, customer_id)
values (33, '6/7/2019', 49);
insert into Orders (order_id, order_date, customer_id)
values (34, '6/3/2019', 49);
insert into Orders (order_id, order_date, customer_id)
values (35, '5/14/2019', 69);
insert into Orders (order_id, order_date, customer_id)
values (36, '4/28/2019', 10);
insert into Orders (order_id, order_date, customer_id)
values (37, '12/29/2018', 66);
insert into Orders (order_id, order_date, customer_id)
values (38, '3/22/2019', 93);
insert into Orders (order_id, order_date, customer_id)
values (39, '3/15/2019', 24);
insert into Orders (order_id, order_date, customer_id)
values (40, '2/11/2019', 18);
insert into Orders (order_id, order_date, customer_id)
values (41, '3/12/2019', 5);
insert into Orders (order_id, order_date, customer_id)
values (42, '10/1/2019', 10);
insert into Orders (order_id, order_date, customer_id)
values (43, '7/23/2019', 82);
insert into Orders (order_id, order_date, customer_id)
values (44, '10/22/2019', 38);
insert into Orders (order_id, order_date, customer_id)
values (45, '5/19/2019', 27);
insert into Orders (order_id, order_date, customer_id)
values (46, '4/8/2019', 55);
insert into Orders (order_id, order_date, customer_id)
values (47, '11/14/2018', 31);
insert into Orders (order_id, order_date, customer_id)
values (48, '7/7/2019', 67);
insert into Orders (order_id, order_date, customer_id)
values (49, '12/4/2018', 96);
insert into Orders (order_id, order_date, customer_id)
values (50, '7/1/2019', 92);
insert into Orders (order_id, order_date, customer_id)
values (51, '2/16/2019', 2);
insert into Orders (order_id, order_date, customer_id)
values (52, '3/4/2019', 53);
insert into Orders (order_id, order_date, customer_id)
values (53, '5/24/2019', 13);
insert into Orders (order_id, order_date, customer_id)
values (54, '4/5/2019', 56);
insert into Orders (order_id, order_date, customer_id)
values (55, '5/23/2019', 62);
insert into Orders (order_id, order_date, customer_id)
values (56, '5/5/2019', 96);
insert into Orders (order_id, order_date, customer_id)
values (57, '5/5/2019', 57);
insert into Orders (order_id, order_date, customer_id)
values (58, '3/27/2019', 5);
insert into Orders (order_id, order_date, customer_id)
values (59, '9/20/2019', 98);
insert into Orders (order_id, order_date, customer_id)
values (60, '6/24/2019', 91);
insert into Orders (order_id, order_date, customer_id)
values (61, '11/11/2018', 68);
insert into Orders (order_id, order_date, customer_id)
values (62, '1/1/2019', 2);
insert into Orders (order_id, order_date, customer_id)
values (63, '6/21/2019', 60);
insert into Orders (order_id, order_date, customer_id)
values (64, '5/22/2019', 43);
insert into Orders (order_id, order_date, customer_id)
values (65, '6/1/2019', 4);
insert into Orders (order_id, order_date, customer_id)
values (66, '8/15/2019', 12);
insert into Orders (order_id, order_date, customer_id)
values (67, '5/31/2019', 96);
insert into Orders (order_id, order_date, customer_id)
values (68, '11/13/2018', 53);
insert into Orders (order_id, order_date, customer_id)
values (69, '3/11/2019', 51);
insert into Orders (order_id, order_date, customer_id)
values (70, '6/14/2019', 40);
insert into Orders (order_id, order_date, customer_id)
values (71, '6/27/2019', 61);
insert into Orders (order_id, order_date, customer_id)
values (72, '5/4/2019', 72);
insert into Orders (order_id, order_date, customer_id)
values (73, '11/6/2018', 16);
insert into Orders (order_id, order_date, customer_id)
values (74, '1/29/2019', 6);
insert into Orders (order_id, order_date, customer_id)
values (75, '3/28/2019', 98);
insert into Orders (order_id, order_date, customer_id)
values (76, '4/7/2019', 35);
insert into Orders (order_id, order_date, customer_id)
values (77, '3/17/2019', 85);
insert into Orders (order_id, order_date, customer_id)
values (78, '3/20/2019', 66);
insert into Orders (order_id, order_date, customer_id)
values (79, '7/29/2019', 57);
insert into Orders (order_id, order_date, customer_id)
values (80, '2/20/2019', 87);
insert into Orders (order_id, order_date, customer_id)
values (81, '1/7/2019', 65);
insert into Orders (order_id, order_date, customer_id)
values (82, '11/19/2018', 54);
insert into Orders (order_id, order_date, customer_id)
values (83, '11/23/2018', 84);
insert into Orders (order_id, order_date, customer_id)
values (84, '12/22/2018', 100);
insert into Orders (order_id, order_date, customer_id)
values (85, '3/12/2019', 33);
insert into Orders (order_id, order_date, customer_id)
values (86, '1/4/2019', 85);
insert into Orders (order_id, order_date, customer_id)
values (87, '11/7/2018', 67);
insert into Orders (order_id, order_date, customer_id)
values (88, '11/10/2018', 91);
insert into Orders (order_id, order_date, customer_id)
values (89, '2/28/2019', 43);
insert into Orders (order_id, order_date, customer_id)
values (90, '3/31/2019', 50);
insert into Orders (order_id, order_date, customer_id)
values (91, '12/28/2018', 45);
insert into Orders (order_id, order_date, customer_id)
values (92, '7/22/2019', 82);
insert into Orders (order_id, order_date, customer_id)
values (93, '4/26/2019', 88);
insert into Orders (order_id, order_date, customer_id)
values (94, '12/9/2018', 88);
insert into Orders (order_id, order_date, customer_id)
values (95, '6/11/2019', 95);
insert into Orders (order_id, order_date, customer_id)
values (96, '2/26/2019', 58);
insert into Orders (order_id, order_date, customer_id)
values (97, '9/24/2019', 9);
insert into Orders (order_id, order_date, customer_id)
values (98, '5/3/2019', 72);
insert into Orders (order_id, order_date, customer_id)
values (99, '10/25/2019', 22);
insert into Orders (order_id, order_date, customer_id)
values (100, '4/10/2019', 33);
insert into Orders (order_id, order_date, customer_id)
values (101, '6/8/2019', 91);
insert into Orders (order_id, order_date, customer_id)
values (102, '11/4/2018', 48);
insert into Orders (order_id, order_date, customer_id)
values (103, '8/7/2019', 22);
insert into Orders (order_id, order_date, customer_id)
values (104, '5/20/2019', 42);
insert into Orders (order_id, order_date, customer_id)
values (105, '9/29/2019', 24);
insert into Orders (order_id, order_date, customer_id)
values (106, '11/30/2018', 70);
insert into Orders (order_id, order_date, customer_id)
values (107, '12/8/2018', 7);
insert into Orders (order_id, order_date, customer_id)
values (108, '3/9/2019', 77);
insert into Orders (order_id, order_date, customer_id)
values (109, '8/7/2019', 52);
insert into Orders (order_id, order_date, customer_id)
values (110, '12/23/2018', 5);
insert into Orders (order_id, order_date, customer_id)
values (111, '3/26/2019', 57);
insert into Orders (order_id, order_date, customer_id)
values (112, '5/10/2019', 90);
insert into Orders (order_id, order_date, customer_id)
values (113, '1/5/2019', 60);
insert into Orders (order_id, order_date, customer_id)
values (114, '2/11/2019', 77);
insert into Orders (order_id, order_date, customer_id)
values (115, '9/19/2019', 92);
insert into Orders (order_id, order_date, customer_id)
values (116, '4/3/2019', 23);
insert into Orders (order_id, order_date, customer_id)
values (117, '9/29/2019', 63);
insert into Orders (order_id, order_date, customer_id)
values (118, '11/26/2018', 35);
insert into Orders (order_id, order_date, customer_id)
values (119, '5/8/2019', 49);
insert into Orders (order_id, order_date, customer_id)
values (120, '9/17/2019', 9);
insert into Orders (order_id, order_date, customer_id)
values (121, '5/14/2019', 38);
insert into Orders (order_id, order_date, customer_id)
values (122, '9/6/2019', 51);
insert into Orders (order_id, order_date, customer_id)
values (123, '10/2/2019', 25);
insert into Orders (order_id, order_date, customer_id)
values (124, '8/19/2019', 78);
insert into Orders (order_id, order_date, customer_id)
values (125, '1/2/2019', 15);
insert into Orders (order_id, order_date, customer_id)
values (126, '5/1/2019', 76);
insert into Orders (order_id, order_date, customer_id)
values (127, '1/14/2019', 22);
insert into Orders (order_id, order_date, customer_id)
values (128, '2/12/2019', 50);
insert into Orders (order_id, order_date, customer_id)
values (129, '7/2/2019', 81);
insert into Orders (order_id, order_date, customer_id)
values (130, '1/30/2019', 85);
insert into Orders (order_id, order_date, customer_id)
values (131, '7/4/2019', 29);
insert into Orders (order_id, order_date, customer_id)
values (132, '8/29/2019', 47);
insert into Orders (order_id, order_date, customer_id)
values (133, '3/11/2019', 57);
insert into Orders (order_id, order_date, customer_id)
values (134, '7/21/2019', 3);
insert into Orders (order_id, order_date, customer_id)
values (135, '3/26/2019', 11);
insert into Orders (order_id, order_date, customer_id)
values (136, '4/29/2019', 58);
insert into Orders (order_id, order_date, customer_id)
values (137, '5/4/2019', 14);
insert into Orders (order_id, order_date, customer_id)
values (138, '2/23/2019', 97);
insert into Orders (order_id, order_date, customer_id)
values (139, '3/4/2019', 56);
insert into Orders (order_id, order_date, customer_id)
values (140, '4/9/2019', 93);
insert into Orders (order_id, order_date, customer_id)
values (141, '5/13/2019', 4);
insert into Orders (order_id, order_date, customer_id)
values (142, '10/26/2019', 41);
insert into Orders (order_id, order_date, customer_id)
values (143, '6/30/2019', 47);
insert into Orders (order_id, order_date, customer_id)
values (144, '4/1/2019', 54);
insert into Orders (order_id, order_date, customer_id)
values (145, '10/12/2019', 21);
insert into Orders (order_id, order_date, customer_id)
values (146, '10/27/2019', 50);
insert into Orders (order_id, order_date, customer_id)
values (147, '5/18/2019', 64);
insert into Orders (order_id, order_date, customer_id)
values (148, '6/26/2019', 85);
insert into Orders (order_id, order_date, customer_id)
values (149, '11/1/2018', 74);
insert into Orders (order_id, order_date, customer_id)
values (150, '7/8/2019', 19);
insert into Orders (order_id, order_date, customer_id)
values (151, '10/27/2019', 42);
insert into Orders (order_id, order_date, customer_id)
values (152, '1/13/2019', 66);
insert into Orders (order_id, order_date, customer_id)
values (153, '8/26/2019', 25);
insert into Orders (order_id, order_date, customer_id)
values (154, '9/16/2019', 16);
insert into Orders (order_id, order_date, customer_id)
values (155, '1/11/2019', 67);
insert into Orders (order_id, order_date, customer_id)
values (156, '4/20/2019', 74);
insert into Orders (order_id, order_date, customer_id)
values (157, '8/1/2019', 17);
insert into Orders (order_id, order_date, customer_id)
values (158, '12/25/2018', 74);
insert into Orders (order_id, order_date, customer_id)
values (159, '7/29/2019', 33);
insert into Orders (order_id, order_date, customer_id)
values (160, '1/16/2019', 61);
insert into Orders (order_id, order_date, customer_id)
values (161, '10/16/2019', 62);
insert into Orders (order_id, order_date, customer_id)
values (162, '10/19/2019', 89);
insert into Orders (order_id, order_date, customer_id)
values (163, '3/15/2019', 78);
insert into Orders (order_id, order_date, customer_id)
values (164, '1/1/2019', 44);
insert into Orders (order_id, order_date, customer_id)
values (165, '10/1/2019', 93);
insert into Orders (order_id, order_date, customer_id)
values (166, '3/11/2019', 97);
insert into Orders (order_id, order_date, customer_id)
values (167, '11/10/2018', 41);
insert into Orders (order_id, order_date, customer_id)
values (168, '5/21/2019', 47);
insert into Orders (order_id, order_date, customer_id)
values (169, '9/30/2019', 85);
insert into Orders (order_id, order_date, customer_id)
values (170, '4/11/2019', 13);
insert into Orders (order_id, order_date, customer_id)
values (171, '10/3/2019', 100);
insert into Orders (order_id, order_date, customer_id)
values (172, '4/9/2019', 82);
insert into Orders (order_id, order_date, customer_id)
values (173, '1/12/2019', 65);
insert into Orders (order_id, order_date, customer_id)
values (174, '8/15/2019', 51);
insert into Orders (order_id, order_date, customer_id)
values (175, '3/6/2019', 97);
insert into Orders (order_id, order_date, customer_id)
values (176, '1/7/2019', 48);
insert into Orders (order_id, order_date, customer_id)
values (177, '2/22/2019', 90);
insert into Orders (order_id, order_date, customer_id)
values (178, '11/25/2018', 9);
insert into Orders (order_id, order_date, customer_id)
values (179, '2/20/2019', 66);
insert into Orders (order_id, order_date, customer_id)
values (180, '3/20/2019', 31);
insert into Orders (order_id, order_date, customer_id)
values (181, '11/26/2018', 14);
insert into Orders (order_id, order_date, customer_id)
values (182, '4/19/2019', 48);
insert into Orders (order_id, order_date, customer_id)
values (183, '3/17/2019', 37);
insert into Orders (order_id, order_date, customer_id)
values (184, '5/3/2019', 59);
insert into Orders (order_id, order_date, customer_id)
values (185, '10/26/2019', 78);
insert into Orders (order_id, order_date, customer_id)
values (186, '3/27/2019', 72);
insert into Orders (order_id, order_date, customer_id)
values (187, '3/18/2019', 32);
insert into Orders (order_id, order_date, customer_id)
values (188, '2/5/2019', 79);
insert into Orders (order_id, order_date, customer_id)
values (189, '3/22/2019', 69);
insert into Orders (order_id, order_date, customer_id)
values (190, '2/12/2019', 9);
insert into Orders (order_id, order_date, customer_id)
values (191, '11/28/2018', 17);
insert into Orders (order_id, order_date, customer_id)
values (192, '6/11/2019', 84);
insert into Orders (order_id, order_date, customer_id)
values (193, '5/19/2019', 29);
insert into Orders (order_id, order_date, customer_id)
values (194, '7/11/2019', 57);
insert into Orders (order_id, order_date, customer_id)
values (195, '10/18/2019', 92);
insert into Orders (order_id, order_date, customer_id)
values (196, '7/27/2019', 33);
insert into Orders (order_id, order_date, customer_id)
values (197, '11/21/2018', 93);
insert into Orders (order_id, order_date, customer_id)
values (198, '5/31/2019', 50);
insert into Orders (order_id, order_date, customer_id)
values (199, '12/28/2018', 2);
insert into Orders (order_id, order_date, customer_id)
values (200, '4/22/2019', 70);
insert into Orders (order_id, order_date, customer_id)
values (201, '6/7/2019', 26);
insert into Orders (order_id, order_date, customer_id)
values (202, '11/28/2018', 26);
insert into Orders (order_id, order_date, customer_id)
values (203, '7/24/2019', 22);
insert into Orders (order_id, order_date, customer_id)
values (204, '11/8/2018', 75);
insert into Orders (order_id, order_date, customer_id)
values (205, '3/16/2019', 42);
insert into Orders (order_id, order_date, customer_id)
values (206, '7/21/2019', 70);
insert into Orders (order_id, order_date, customer_id)
values (207, '2/14/2019', 28);
insert into Orders (order_id, order_date, customer_id)
values (208, '4/22/2019', 53);
insert into Orders (order_id, order_date, customer_id)
values (209, '6/17/2019', 24);
insert into Orders (order_id, order_date, customer_id)
values (210, '1/30/2019', 38);
insert into Orders (order_id, order_date, customer_id)
values (211, '11/23/2018', 12);
insert into Orders (order_id, order_date, customer_id)
values (212, '3/31/2019', 72);
insert into Orders (order_id, order_date, customer_id)
values (213, '11/16/2018', 88);
insert into Orders (order_id, order_date, customer_id)
values (214, '2/20/2019', 76);
insert into Orders (order_id, order_date, customer_id)
values (215, '11/7/2018', 65);
insert into Orders (order_id, order_date, customer_id)
values (216, '11/24/2018', 24);
insert into Orders (order_id, order_date, customer_id)
values (217, '1/14/2019', 15);
insert into Orders (order_id, order_date, customer_id)
values (218, '8/24/2019', 100);
insert into Orders (order_id, order_date, customer_id)
values (219, '11/30/2018', 44);
insert into Orders (order_id, order_date, customer_id)
values (220, '5/11/2019', 71);
insert into Orders (order_id, order_date, customer_id)
values (221, '2/24/2019', 15);
insert into Orders (order_id, order_date, customer_id)
values (222, '11/25/2018', 1);
insert into Orders (order_id, order_date, customer_id)
values (223, '6/18/2019', 49);
insert into Orders (order_id, order_date, customer_id)
values (224, '7/18/2019', 39);
insert into Orders (order_id, order_date, customer_id)
values (225, '11/24/2018', 3);
insert into Orders (order_id, order_date, customer_id)
values (226, '2/6/2019', 85);
insert into Orders (order_id, order_date, customer_id)
values (227, '12/30/2018', 48);
insert into Orders (order_id, order_date, customer_id)
values (228, '5/10/2019', 1);
insert into Orders (order_id, order_date, customer_id)
values (229, '3/31/2019', 40);
insert into Orders (order_id, order_date, customer_id)
values (230, '1/14/2019', 94);
insert into Orders (order_id, order_date, customer_id)
values (231, '12/12/2018', 65);
insert into Orders (order_id, order_date, customer_id)
values (232, '7/18/2019', 80);
insert into Orders (order_id, order_date, customer_id)
values (233, '9/20/2019', 43);
insert into Orders (order_id, order_date, customer_id)
values (234, '6/5/2019', 100);
insert into Orders (order_id, order_date, customer_id)
values (235, '6/22/2019', 17);
insert into Orders (order_id, order_date, customer_id)
values (236, '10/19/2019', 40);
insert into Orders (order_id, order_date, customer_id)
values (237, '11/11/2018', 80);
insert into Orders (order_id, order_date, customer_id)
values (238, '11/13/2018', 10);
insert into Orders (order_id, order_date, customer_id)
values (239, '5/10/2019', 61);
insert into Orders (order_id, order_date, customer_id)
values (240, '5/22/2019', 34);
insert into Orders (order_id, order_date, customer_id)
values (241, '4/3/2019', 52);
insert into Orders (order_id, order_date, customer_id)
values (242, '9/14/2019', 13);
insert into Orders (order_id, order_date, customer_id)
values (243, '2/23/2019', 29);
insert into Orders (order_id, order_date, customer_id)
values (244, '2/15/2019', 80);
insert into Orders (order_id, order_date, customer_id)
values (245, '7/17/2019', 98);
insert into Orders (order_id, order_date, customer_id)
values (246, '4/25/2019', 78);
insert into Orders (order_id, order_date, customer_id)
values (247, '5/21/2019', 34);
insert into Orders (order_id, order_date, customer_id)
values (248, '11/19/2018', 97);
insert into Orders (order_id, order_date, customer_id)
values (249, '5/8/2019', 79);
insert into Orders (order_id, order_date, customer_id)
values (250, '9/14/2019', 72);
insert into Orders (order_id, order_date, customer_id)
values (251, '2/25/2019', 84);
insert into Orders (order_id, order_date, customer_id)
values (252, '1/13/2019', 30);
insert into Orders (order_id, order_date, customer_id)
values (253, '1/14/2019', 76);
insert into Orders (order_id, order_date, customer_id)
values (254, '9/21/2019', 95);
insert into Orders (order_id, order_date, customer_id)
values (255, '10/30/2019', 58);
insert into Orders (order_id, order_date, customer_id)
values (256, '2/6/2019', 46);
insert into Orders (order_id, order_date, customer_id)
values (257, '1/6/2019', 96);
insert into Orders (order_id, order_date, customer_id)
values (258, '11/18/2018', 77);
insert into Orders (order_id, order_date, customer_id)
values (259, '3/1/2019', 90);
insert into Orders (order_id, order_date, customer_id)
values (260, '6/25/2019', 40);
insert into Orders (order_id, order_date, customer_id)
values (261, '1/9/2019', 54);
insert into Orders (order_id, order_date, customer_id)
values (262, '7/12/2019', 95);
insert into Orders (order_id, order_date, customer_id)
values (263, '2/14/2019', 75);
insert into Orders (order_id, order_date, customer_id)
values (264, '3/21/2019', 13);
insert into Orders (order_id, order_date, customer_id)
values (265, '6/13/2019', 66);
insert into Orders (order_id, order_date, customer_id)
values (266, '7/28/2019', 45);
insert into Orders (order_id, order_date, customer_id)
values (267, '10/10/2019', 78);
insert into Orders (order_id, order_date, customer_id)
values (268, '11/8/2018', 71);
insert into Orders (order_id, order_date, customer_id)
values (269, '4/24/2019', 62);
insert into Orders (order_id, order_date, customer_id)
values (270, '2/24/2019', 4);
insert into Orders (order_id, order_date, customer_id)
values (271, '10/8/2019', 17);
insert into Orders (order_id, order_date, customer_id)
values (272, '8/21/2019', 36);
insert into Orders (order_id, order_date, customer_id)
values (273, '8/27/2019', 76);
insert into Orders (order_id, order_date, customer_id)
values (274, '6/10/2019', 37);
insert into Orders (order_id, order_date, customer_id)
values (275, '6/15/2019', 9);
insert into Orders (order_id, order_date, customer_id)
values (276, '5/23/2019', 2);
insert into Orders (order_id, order_date, customer_id)
values (277, '3/23/2019', 93);
insert into Orders (order_id, order_date, customer_id)
values (278, '8/27/2019', 74);
insert into Orders (order_id, order_date, customer_id)
values (279, '5/15/2019', 28);
insert into Orders (order_id, order_date, customer_id)
values (280, '6/14/2019', 28);
insert into Orders (order_id, order_date, customer_id)
values (281, '4/23/2019', 45);
insert into Orders (order_id, order_date, customer_id)
values (282, '4/27/2019', 67);
insert into Orders (order_id, order_date, customer_id)
values (283, '10/22/2019', 53);
insert into Orders (order_id, order_date, customer_id)
values (284, '1/7/2019', 8);
insert into Orders (order_id, order_date, customer_id)
values (285, '7/20/2019', 10);
insert into Orders (order_id, order_date, customer_id)
values (286, '11/9/2018', 92);
insert into Orders (order_id, order_date, customer_id)
values (287, '10/16/2019', 80);
insert into Orders (order_id, order_date, customer_id)
values (288, '2/15/2019', 46);
insert into Orders (order_id, order_date, customer_id)
values (289, '2/16/2019', 68);
insert into Orders (order_id, order_date, customer_id)
values (290, '7/11/2019', 32);
insert into Orders (order_id, order_date, customer_id)
values (291, '10/25/2019', 21);
insert into Orders (order_id, order_date, customer_id)
values (292, '10/26/2019', 86);
insert into Orders (order_id, order_date, customer_id)
values (293, '3/14/2019', 37);
insert into Orders (order_id, order_date, customer_id)
values (294, '3/19/2019', 29);
insert into Orders (order_id, order_date, customer_id)
values (295, '2/2/2019', 87);
insert into Orders (order_id, order_date, customer_id)
values (296, '1/1/2019', 66);
insert into Orders (order_id, order_date, customer_id)
values (297, '7/28/2019', 6);
insert into Orders (order_id, order_date, customer_id)
values (298, '6/5/2019', 25);
insert into Orders (order_id, order_date, customer_id)
values (299, '1/19/2019', 57);
insert into Orders (order_id, order_date, customer_id)
values (300, '2/18/2019', 54);

SET IDENTITY_INSERT Orders OFF;