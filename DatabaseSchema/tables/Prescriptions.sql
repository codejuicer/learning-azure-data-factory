-------------------------------------------------------------------------------------
-- Note: All data in this file was generated at https://mockaroo.com/.
-- Thank you for existing Mockaroo!
-------------------------------------------------------------------------------------
drop table if exists Prescriptions;
create table Prescriptions
(
    prescription_id   INT IDENTITY PRIMARY KEY,
    prescription_name VARCHAR(100),
    chemical_name     VARCHAR(200),
    ndc_code          VARCHAR(50),
    unit_price        DECIMAL(18, 2)
);

SET IDENTITY_INSERT Prescriptions ON
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (1, 'Rouge Dior 470 Pearly Pink Serum', 'OCTINOXATE', '61957-1068', 223.39);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (2, 'BioTox Bac',
        'Cochlearia armoracia, Taraxacum officinale, Tabebuia impetiginosa, Brucella abortus, Colibacillinum cum natrum muriaticum, Germanium, Pertussinum, Anthracinum,',
        '50181-0045', 466.15);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (3, 'Flawless Tinted Moisturizer Medium', 'Octinoxate, Octisalate, Avobenzone', '24453-199', 486.55);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (4, 'CLOMIPRAMINE HYDROCHLORIDE', 'Clomipramine Hydrochloride', '51672-4012', 473.05);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (5, 'Ciprofloxacin', 'Ciprofloxacin', '63304-710', 273.67);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (6, 'Orajel Instant Pain Relief', 'Benzocaine', '10237-708', 405.37);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (7, 'Lansoprazole', 'Lansoprazole', '0093-7350', 154.66);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (8, 'Neutrogena Pure and Free Baby', 'Titanium Dioxide and Zinc Oxide', '10812-431', 415.32);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (9, 'Midazolam Hydrochloride', 'MIDAZOLAM HYDROCHLORIDE', '55154-4743', 195.24);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (10, 'Promethazine Hydrochloride', 'PROMETHAZINE HYDROCHLORIDE', '0409-2312', 368.78);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (11, 'TIMENTIN', 'ticarcillin disodium and clavulanate potassium', '0029-6571', 234.52);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (12, 'Hydralazine Hydrochloride', 'hydralazine hydrochloride', '0378-5114', 47.08);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (13, 'SENSAI TRIPLE TOUCH COMPACT TC01', 'TITANIUM DIOXIDE, ZINC OXIDE and OCTINOXATE', '64159-7087', 330.25);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (14, 'antacid', 'Aluminum hydroxide, Magnesium hydroxide, Simethicone', '55312-851', 443.73);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (15, 'Diazepam', 'diazepam', '66336-033', 120.39);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (16, 'ANTIBACTERIAL FOAMING', 'TRICLOSAN', '41520-170', 496.65);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (17, 'White Birch', 'White Birch', '36987-2551', 126.76);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (18, 'Retin-A MICRO', 'Tretinoin', '0062-0204', 372.31);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (19, 'Clotrimazole', 'Clotrimazole', '0054-8146', 252.97);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (20, 'Tineacide', 'Undecylenic Acid', '65373-500', 489.99);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (21, 'Doxazosin', 'Doxazosin Mesylate', '49999-206', 237.16);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (22, 'ATORVASTATIN CALCIUM', 'ATORVASTATIN CALCIUM', '60429-325', 277.69);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (23, 'Candy Apple Anti Bacterial Foaming Hand', 'TRICLOSAN', '59021-001', 281.58);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (24, 'ACETICUM ACIDUM', 'ACETICUM ACIDUM', '60512-6154', 299.62);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (25, 'Chlorpromazine Hydrochloride', 'Chlorpromazine Hydrochloride', '52125-062', 34.16);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (26, 'High Protection Sun SPF 30', 'Titanium Dioxide', '44924-101', 197.58);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (27, 'SaniSuds Foam Hand Sanitizer', 'Benzalkonium Chloride', '48417-750', 222.19);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (28, 'PANADOL', 'acetaminophen', '0135-0135', 183.0);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (29, 'Bupropion Hydrochloride', 'Bupropion Hydrochloride', '51079-391', 392.57);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (30, 'SULINDAC', 'sulindac', '53808-0302', 433.31);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (31, 'SunscreenSPF 30', 'Octinoxate, Octisalate, Oxybenzone, Titanium Dioxide', '75857-2305', 39.56);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (32, 'Bengay Ultra Strength', 'Menthol', '58232-6001', 319.49);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (33, 'Acetaminophen PM', 'Acetaminophen, Diphenhydramine HCl', '59726-213', 282.23);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (34, 'Crinone', 'progesterone', '55056-0808', 412.5);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (35, 'Eve Lom Brilliant Cover Concealer Broad Spectrum SPF 15 Sunscreen', 'Titanium Dioxide and Octinoxate',
        '61601-1141', 172.16);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (36, 'ALLIUM CEPA', 'ONION', '54973-2901', 50.19);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (37, 'Hydrocodone bitartrate and ibuprofen', 'Hydrocodone bitartrate and ibuprofen', '57664-281', 216.49);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (38, 'Hydrocodone Bitartrate And Acetaminophen', 'Hydrocodone Bitartrate And Acetaminophen', '66336-406',
        147.72);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (39, 'Penicillin V Potassium', 'Penicillin V Potassium', '67253-200', 346.56);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (40, 'Desmopressin Acetate', 'Desmopressin Acetate', '0615-7549', 107.23);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (41, 'CENTER-AL - ULMUS AMERICANA POLLEN', 'Allergenic Extracts Alum Precipitated', '0268-0069', 340.28);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (42, 'Home Care Fluoride', 'Stannous Fluoride', '55346-0404', 471.41);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (43, 'Tinted Moisturizer Matte Broad Spectrum SPF 45', 'octinoxate, octisalate, titanium dioxide, zinc oxide',
        '10477-7603', 231.9);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (44, 'Piroxicam', 'Piroxicam', '42291-677', 204.01);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (45, 'stay awake', 'Caffeine', '56062-409', 98.68);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (46, 'Probenecid', 'Probenecid', '0527-1367', 82.09);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (47, 'pain relief', 'Acetaminophen', '37808-700', 372.27);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (48, 'Valacyclovir', 'Valacyclovir', '63629-4837', 58.92);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (49, 'Oxybutynin Chloride', 'Oxybutynin Chloride', '62175-270', 341.43);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (50, 'potassium chloride', 'potassium chloride', '76237-279', 62.77);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (51, 'Thioridazine Hydrochloride', 'Thioridazine Hydrochloride', '49349-130', 153.98);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (52, 'Unburn', 'Lidocaine hydrochloride', '59898-500', 187.24);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (53, 'Spironolactone', 'spironolactone', '50436-5015', 184.39);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (54, 'Pioglitazone and Metformin Hydrocholride', 'pioglitazone and metformin hydrochloride', '42291-693',
        362.21);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (55, 'PROPOLIS 90 AMPOULE', 'ZINC OXIDE', '59535-9001', 196.99);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (56, 'bareMinerals READY Touch Up Veil Broad Spectrum SPF 15', 'Titanium Dioxide and Zinc oxide', '98132-305',
        313.36);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (57, 'Anagallis Taraxacum', 'Anagallis Taraxacum', '48951-1030', 186.44);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (58, 'Amoxicillin', 'Amoxicillin', '49999-015', 498.33);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (59, 'HAND AND NATURE SANITIZER', 'Alcohol', '51346-228', 436.37);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (60, 'Dilantin', 'PHENYTOIN', '49349-495', 90.84);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (61, 'Rocky Mountain Juniper', 'Rocky Mountain Juniper', '36987-2707', 338.76);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (62, 'Forest Sap Patch', 'TOPICAL STARCH', '34690-7001', 237.34);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (63, 'Levisticum Quercus', 'Levisticum Quercus', '48951-6030', 490.12);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (64, 'Indomethacin', 'Indomethacin', '66116-529', 296.41);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (65, 'Dove', 'Aluminum Zirconium Tetrachlorohydrex GLY', '64942-1348', 59.42);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (66, 'OP Protective Gel 15', 'octinoxate, octisalate', '62802-039', 97.07);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (67, 'white faced hornet hymenoptera venom', 'white faced hornet hymenoptera venom multidose', '65044-9951',
        176.34);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (68, 'Fluocinolone Acetonide', 'Fluocinolone Acetonide', '52565-020', 68.47);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (69, 'Olanzapine', 'Olanzapine', '60429-621', 128.5);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (70, 'Cymbalta', 'Duloxetine hydrochloride', '55154-1818', 233.49);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (71, 'Morphine Sulfate', 'Morphine Sulfate', '52533-160', 226.22);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (72, 'Betamethasone Valerate', 'Betamethasone Valerate', '45802-053', 274.92);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (73, 'Leg Cramp Away', 'Calc phos, Mag phos, Nux vom, Rhus tox', '68703-088', 183.87);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (74, 'MIRTAZAPINE', 'MIRTAZAPINE', '16590-154', 255.12);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (75, 'TRAMADOL HYDROCHLORIDE AND ACETAMINOPHEN', 'Tramadol hydrochloride and Acetaminophen', '42571-119',
        138.14);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (76, 'levofloxacin', 'levofloxacin', '65841-691', 167.94);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (77, 'Midodrine Hydrochloride', 'midodrine hydrochloride', '0378-1902', 78.22);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (78, 'Suprenza', 'phentermine hydrochloride', '24090-720', 445.92);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (79, 'clindamycin phosphate', 'clindamycin phosphate', '0168-0202', 141.86);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (80, 'CANTHARIS', 'LYTTA VESICATORIA', '54973-2909', 309.47);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (81, 'Naproxen', 'Naproxen', '24236-995', 316.91);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (82, 'Ranitidine', 'Ranitidine Hydrochloride', '0440-2300', 85.97);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (83, 'AMILORIDE HYDROCHLORIDE', 'amiloride hydrochloride', '54868-5214', 282.3);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (84, 'Hydrogen Peroxide', 'Hydrogen Peroxide', '70253-214', 203.73);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (85, 'Phineas and Ferb Sun Smacker SPF 24 Vanilla Vacation', 'OCTINOXATE, OXYBENZONE, PADIMATE O', '50051-0015',
        271.92);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (86, 'Zenzedi', 'Dextroamphetamine Sulfate', '24338-855', 293.51);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (87, 'Whitening Repairing BB', 'Allantoin', '59535-9101', 78.04);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (88, 'Hand Sanitizer', 'Ethyl Alcohol', '11344-646', 368.43);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (89, 'stool softener', 'Docusate sodium', '42507-486', 436.62);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (90, 'METROGEL', 'metronidazole', '0299-3820', 84.55);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (91, 'Purell Advanced Hand Sanitizer Tranquil Sunset', 'ALCOHOL', '21749-143', 307.0);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (92, 'Ketorolac Tromethamine', 'Ketorolac Tromethamine', '0641-6041', 253.68);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (93, 'Monkey Holding Peach Brand', 'Camphor,Menthol,Methyl Salicylate', '57373-108', 461.79);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (94, 'Hydrocodone Bitartrate And Acetaminophen', 'Hydrocodone Bitartrate And Acetaminophen', '54569-6438',
        415.5);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (95, 'COLESTIPOL HYDROCHLORIDE', 'COLESTIPOL HYDROCHLORIDE', '0115-5211', 412.98);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (96, 'Senna Plus', 'standardized senna concentrate and docusate sodium', '55154-5889', 293.04);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (97, 'Dover Sudanyl PE', 'PHENYLEPHRINE HYDROCHLORIDE', '47682-162', 390.51);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (98, 'Sun Shades Lip Balm', 'Avobenzone 2.5%, Octinoxate 6%, Oxybenzone 3%', '54473-245', 415.85);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (99, 'PROMETHAZINE DM', 'Dextromethorphan Hydrobromide and Promethazine Hydrochloride', '68788-9097', 260.06);
insert into Prescriptions (prescription_id, prescription_name, chemical_name, ndc_code, unit_price)
values (100, 'Helminthosporium sativum', 'Helminthosporium sativum', '49643-112', 420.52);
SET IDENTITY_INSERT Prescriptions OFF