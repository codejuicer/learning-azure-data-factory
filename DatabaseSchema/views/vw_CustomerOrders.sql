CREATE OR ALTER VIEW vw_CustomerOrders AS
(
SELECT o.order_id,
       o.order_date,
       c.customer_id,
       first_name,
       last_name,
       email,
       address,
       city,
       state,
       p.prescription_name,
       li.quantity,
       p.unit_price * li.quantity AS [price]

FROM Orders o
         INNER JOIN Customers c on o.customer_id = C.customer_id
         INNER JOIN LineItems li on o.order_id = LI.order_id
         INNER JOIN Prescriptions P on li.prescription_id = P.prescription_id
    );